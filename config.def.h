/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* APPEARANCE|------------------------------------------------------------------------------------------------- */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappx     = 6;        /* gaps between windows */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Iosevka Nerd Font:style=Regular:size=9:antialias=true:autohint=true", "emoji:size=10" };
static const char dmenufont[]       = "Iosevka Nerd Font:size=9";
static const char col_gray1[]       = "#000000";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#b19a90";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

static const char *tags[] = {" "," "," "," ","󰨞 "," ","ﱘ "," ","󰍇"};
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "gnome-calculator", NULL, NULL,     0,	    1,		 -1 },
	{ "Brave",  NULL,       NULL,       1 << 9,       0,           -1 }, 
};

/*LAYOUTS|----------------------------------------------------------------------------------------------------- */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* KEY DEFINITIONS|--------------------------------------------------------------------------------------------- */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* PROGRAMS AND SCRIPTS|---------------------------------------------------------------------------------------- */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-g", "65x31", NULL };
static const char *browser[] = {"brave", NULL};
static const char *filemgr[] = {"st", "-e", "ranger", NULL};
static const char *downvol[] = {"downvol.sh", NULL};
static const char *mutevol[] = {"mutevol.sh", NULL};
static const char *upvol[] = {"upvol.sh", NULL};
static const char *medplaypausecmd[] = {"playerctl", "play-pause", NULL};
static const char *mednextcmd[] = {"playerctl", "next", NULL};
static const char *medprevcmd[] = {"playerctl", "previous", NULL};
static const char *brupcmd[] = {"brightnessctl", "set", "+2%", NULL};
static const char *brdowncmd[] = {"brightnessctl", "set", "2%-", NULL};
static const char *share[] = {"share.sh", NULL};
static const char *getpath[] = {"getpath.sh", NULL};
static const char *emoji[] = {"dmenu-emoji.sh", NULL};
static const char *scrncap[] = {"scrncap.sh", NULL};
static const char *scrnsel[] = {"scrnsel.sh", NULL};
static const char *clipmenucmd[] = {"clipmenu", NULL};
static const char *clipdelcmd[] = {"clearclip.sh", NULL};
static const char *opencmd[] = {"open.sh", NULL};
static const char *suspend[] = {"systemctl", "suspend", NULL};
static const char *netdwm[] = {"networkmanager_dmenu", NULL};
static const char *mail[] = {"thunderbird", NULL};
static const char *encrypt[] = {"panic-encrypt.sh", NULL};

/* KEYBINDINGS|------------------------------------------------------------------------------------------------ */
static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
    { MODKEY,                       XK_grave,  togglescratch,  {.v = scratchpadcmd } },
	{MODKEY|ShiftMask,	    XK_p,	   spawn,	   {.v = encrypt } },
	{ MODKEY,                       XK_b,      spawn,          {.v = browser } },
	{ MODKEY,			XK_m,	   spawn,	   {.v = mail    }},
	{ MODKEY,                       XK_e,      spawn,          {.v = filemgr} },
	{ MODKEY,			XK_o,	   spawn,		{.v = opencmd}},
	{ MODKEY,                       XK_period, spawn,          {.v = emoji} },
	{ MODKEY|ShiftMask,             XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_s,      spawn,          {.v = share } },
	{MODKEY,			XK_q,	   spawn,	   {.v = getpath}},
	{ MODKEY,                       XK_v,      spawn,          {.v = clipmenucmd }},
	{ MODKEY|ShiftMask,             XK_v,      spawn,          {.v = clipdelcmd } },
	{ MODKEY,                       XK_n,      spawn,          {.v = netdwm } },
	{ 0,                            XF86XK_MonBrightnessUp, spawn, {.v = brupcmd} },
	{ 0,                            XF86XK_MonBrightnessDown, spawn, {.v = brdowncmd} },
	{ 0,                            XF86XK_AudioLowerVolume, spawn, {.v = downvol } },
	{ 0,                            XF86XK_AudioMute, spawn, {.v = mutevol } },
	{ 0,                            XF86XK_AudioRaiseVolume, spawn, {.v = upvol   } },
	{ 0,				XF86XK_AudioPlay,	spawn,	{.v = medplaypausecmd } },
	{ 0,				XF86XK_AudioNext,	spawn, {.v = mednextcmd } },
	{ 0,				XF86XK_AudioPrev,	spawn,	{.v = medprevcmd } },
	{ 0,                            XK_Print,   spawn,          {.v = scrncap } },
	{ ShiftMask,                    XK_Print,   spawn,          {.v = scrnsel } },
	{ MODKEY,                       XK_Escape,  spawn,          {.v = suspend } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
    { MODKEY,                       XK_s,      togglesticky,   {0} },	
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_f,      fullscreenmode, {0} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
    TAGKEYS(                        XK_KP_End,                 0)
	TAGKEYS(                        XK_KP_Down,                1)
	TAGKEYS(                        XK_KP_Page_Down,           2)
	TAGKEYS(                        XK_KP_Left,                3)
	TAGKEYS(                        XK_KP_Begin,               4)
	TAGKEYS(                        XK_KP_Right,               5)
	TAGKEYS(                        XK_KP_Home,                6)
	TAGKEYS(                        XK_KP_Up,                  7)
	TAGKEYS(                        XK_KP_Page_Up,             8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* MOUSE BUTTONS|----------------------------------------------------------------------------------------------- */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
